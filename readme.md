# A blender material library for architectural visualizations

This is a collection of all the materials we have created so that we can re-use them

## Instructions

1. Please include only our own materials or materials that are free for commercial use.
2. Don't forget to add a fake user to any material that you add so that blender doesn't delete it upon saving.

## Todo

1. Fix unnamed materials
2. Prune double materials
3. Make sure materials are as PBR as possible. For more info about PBR read [this](https://www.marmoset.co/posts/physically-based-rendering-and-you-can-too/) 

